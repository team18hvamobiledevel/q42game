var socket = io('/client');
var flicker = true;
var timer;
var controller;
var lastDirection;


// change controller color
socket.on('playerColor', function (color) {
    changeSwipeControllerColor(color);
});
//complete the game by showing the scores
socket.on('completeGame', function (result) {
    startFlicker(result.position, result.score, result.color);
});
//restart the game
socket.on('restartGame', function (result) {
    stopShowingPosition();
});

// reload controller
socket.on('reload', function () {
    location.reload();
});

socket.on('showPosition', function (result) {
    console.log("showposition");
    console.log(result);

    if(result.hide){
        console.log('test');
        $('#line').hide();
        $('#line-horizontal').hide();
        return;
    }else{
        draw_vertical_line(5, window.innerHeight, '#ef5454', result.x+"%", 0);
        draw_horizontal_line(window.innerWidth,5,'#ef5454',0,result.y+"%");
    }


});

socket.on('restartController',function(){
    lastDirection=-1;
    //socket.emit("changeDirection", lastDirection);
});

// for button controller turn around
function changeDirection(value) {
    var direction = "elsewhere";

    if (value === "swipeup") {
        direction = 0;

    } else if (value === "swiperight") {
        direction = 1;
    } else if (value === "swipedown") {
        direction = 2;
    } else if (value === "swipeleft") {
        direction = 3;
    }

    return direction;
}

// change swipe controller colour based on emit from server
function changeSwipeControllerColor(value) {

    var swipeController = document.getElementById('background');
    var myElement = document.getElementById('myElement');
    swipeController.style.backgroundColor = value;
    var direction = document.getElementById('direction');
    direction.style.color = getLighterColor(value, 25);
    myElement.style.color = getLighterColor(value, 25);
}

// calculate lighter color for controller
function getLighterColor(color, percent) {
    var num = parseInt(color.slice(1), 16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
    return "#" + (0x1000000 + (R < 255 ? R < 1 ? 0 : R : 255) * 0x10000 + (G < 255 ? G < 1 ? 0 : G : 255) * 0x100 + (B < 255 ? B < 1 ? 0 : B : 255)).toString(16).slice(1);
}


$(function () {
    var options = {
        preventDefault: true
    };

// create a simple instance
// by default, it only adds horizontal recognizers
    var mc = new Hammer(myElement, options)

    lastDirection = -1;

// let the swipe gesture support all directions.
// this will block the vertical scrolling on a touch-device while on the element
    mc.get('swipe').set({direction: Hammer.DIRECTION_ALL, treshold: 0, pointers: 1})

// listen to events... of swipe controller
    mc.on("swipeleft swiperight swipeup swipedown tap press", function (ev) {
        var gestureDirection, direction;
        myElement.textContent = ev.type + " gesture detected.";
        gestureDirection = ev.type;
        direction = changeDirection(gestureDirection);

        if (lastDirection != direction) {

            socket.emit("changeDirection", direction);
            lastDirection = direction;
        }
        //show glyphicon based on given direction
        if (ev.type === "swipeleft") {
            $(".glyphicon-menu-left").show();
            $(".glyphicon-menu-right").hide();
            $(".glyphicon-menu-down").hide();
            $(".glyphicon-menu-up").hide();

        }
        if (ev.type === "swiperight") {
            $(".glyphicon-menu-right").show();
            $(".glyphicon-menu-left").hide();
            $(".glyphicon-menu-down").hide();
            $(".glyphicon-menu-up").hide();
        }
        if (ev.type === "swipedown") {
            $(".glyphicon-menu-down").show();
            $(".glyphicon-menu-right").hide();
            $(".glyphicon-menu-left").hide();
            $(".glyphicon-menu-up").hide();
        }
        if (ev.type === "swipeup") {
            $(".glyphicon-menu-up").show();
            $(".glyphicon-menu-down").hide();
            $(".glyphicon-menu-right").hide();
            $(".glyphicon-menu-left").hide();
        }

        if (ev.type === "tap") {
            socket.emit("changeDirection", 4);
            console.log(ev.type);

        }
        if (ev.type === "press") {
            socket.emit("showMap");
        }
    });
});


//Function to draw a vertical line
function draw_vertical_line(width, height, linecolor, xpos, ypos) {
    console.log("verticalline");
    $('#line').css({'width':width, 'height':height, 'background-color':linecolor, 'left':xpos, 'top':ypos,'z-index':9999 ,'position':'absolute'}).show();
}
//Function to draw a horizontal line
function draw_horizontal_line(width, height, linecolor, xpos, ypos) {
    console.log("verticalline");
    $('#line-horizontal').css({'width':width, 'height':height, 'background-color':linecolor, 'left':xpos, 'top':ypos, 'z-index':9999,'position':'absolute'}).show();
}

//start a flicker to display the score of each player
function startFlicker(position, score, color) {
    $("#swipeController").hide();
    $(".showposition").html("Your Score: " + "<br/>" + "#" + position + "<br/>" + score);
    var textfield = document.getElementById('showPosition');
    var textposition = document.getElementById('textposition');
    textfield.style.backgroundColor = color;
    textposition.style.color = getLighterColor(color, 25);
    this.flicker = true;
    toggleDiv();
}

//displays the score of each player by the end of the game
function toggleDiv() {
    this.timer = setTimeout(function () {
        $("#showPosition").hide();
        setTimeout(function () {
            $("#showPosition").show();
            toggleDiv();
        }, 500);
    }, 1500);
}

//register users
$(document).ready(function () {
    console.log("ready!");
    socket.emit('registerUser');
    controller = '#swipeController';
});

//stop showing the scores
function stopShowingPosition() {
    clearTimeout(this.timer);
    var stopFlicker = setTimeout(function () {
        $(controller).show();
        $("#showPosition").hide();
    }, 5000);
}

//Use of the arrow keys
$(function () {
    var myElement = document.getElementById('myElement');
    if (!('ontouchstart' in window || navigator.maxTouchPoints)) {
        document.onkeydown = checkArrowKey;
        myElement.innerHTML = ("Use your arrow keys");
    }
});

//check if the controller window is still active
setInterval(function () {
    //console.log(window.isActive ? 'active' : 'inactive');
    checkVisibility();
}, 1000);

//detect the arrow keys when pressed
function checkArrowKey(e) {

    e = e || window.event;
    var direction;
    if (e.keyCode == '38') {
        //Hide position player
        $('#line').hide();
        $('#line-horizontal').hide();

        // up arrow
        myElement.innerHTML = ("Going Up");
        $(".glyphicon-menu-up").show();
        $(".glyphicon-menu-down").hide();
        $(".glyphicon-menu-right").hide();
        $(".glyphicon-menu-left").hide();
        direction = changeDirection('swipeup');
        socket.emit("changeDirection", direction);
    }
    else if (e.keyCode == '40') {
        //Hide position player
        $('#line').hide();
        $('#line-horizontal').hide();
        // down arrow
        myElement.innerHTML = ("Going Down");
        $(".glyphicon-menu-down").show();
        $(".glyphicon-menu-right").hide();
        $(".glyphicon-menu-left").hide();
        $(".glyphicon-menu-up").hide();
        direction = changeDirection('swipedown');
        socket.emit("changeDirection", direction);
    }
    else if (e.keyCode == '37') {
        //Hide position player
        $('#line').hide();
        $('#line-horizontal').hide();
        // left arrow
        myElement.innerHTML = ("Going Left");
        $(".glyphicon-menu-left").show();
        $(".glyphicon-menu-right").hide();
        $(".glyphicon-menu-down").hide();
        $(".glyphicon-menu-up").hide();
        direction = changeDirection('swipeleft');
        socket.emit("changeDirection", direction);
    }
    else if (e.keyCode == '39') {
        //Hide position player
        $('#line').hide();
        $('#line-horizontal').hide();
        // right arrow
        myElement.innerHTML = ("Going Right");
        $(".glyphicon-menu-right").show();
        $(".glyphicon-menu-left").hide();
        $(".glyphicon-menu-down").hide();
        $(".glyphicon-menu-up").hide();
        direction = changeDirection('swiperight');
        socket.emit("changeDirection", direction);
    }
    else if (e.keyCode == '32') {
        //Hide position player
        $('#line').hide();
        $('#line-horizontal').hide();
        // spacebar
        myElement.innerHTML = ("tap detected");
        socket.emit("changeDirection", 4);
    }
    else if (e.keyCode == '77') {
        // M key
        socket.emit("showMap");
    }

}

//check if the controller window is still active
function checkVisibility() {
    var hidden = "hidden";

    // Standards:
    if (hidden in document)
        document.addEventListener("visibilitychange", onchange);
    else if ((hidden = "mozHidden") in document)
        document.addEventListener("mozvisibilitychange", onchange);
    else if ((hidden = "webkitHidden") in document)
        document.addEventListener("webkitvisibilitychange", onchange);
    else if ((hidden = "msHidden") in document)
        document.addEventListener("msvisibilitychange", onchange);
    // IE 9 and lower:
    else if ("onfocusin" in document)
        document.onfocusin = document.onfocusout = onchange;
    // All others:
    else
        window.onpageshow = window.onpagehide
            = window.onfocus = window.onblur = onchange;

    function onchange(evt) {
        var v = "visible", h = "hidden",
            evtMap = {
                focus: v, focusin: v, pageshow: v, blur: h, focusout: h, pagehide: h
            };

        evt = evt || window.event;
        if (evt.type in evtMap) {
            hasVisibility = evtMap[evt.type] != 'hidden';

            if (hasVisibility === false) {
                socket.disconnect();
            }

        }
    }

    // set the initial state (but only if browser supports the Page Visibility API)
    if (document[hidden] !== undefined)
        onchange({type: document[hidden] ? "blur" : "focus"});
}
