// vars for object
var id
    ,x
    ,y
    ,color;

//constructor
function User(id,x,y,color){
    this.id = id;
    this.x = x;
    this.y = y;
    this.color = color;
}


// export the class
module.exports = User;