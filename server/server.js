// needed libs for the game
var express = require('express')
    , app = express()
    , http = require('http').Server(app)
    , path = require('path')
    , userClass = require('./model/user')

// Register socket namespaces
    , io = require('socket.io')(http)
    , displaySocket = io.of('/display')
    , clientSocket = io.of('/client');

// Register assets directories
app.use(express.static(__dirname + '/../client'));
app.use(express.static(__dirname + '/../display'));
// Register routes
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '../client', 'index.html'));
});

app.get('/display', function (req, res) {
    res.sendFile(path.join(__dirname, '../display/canvas.html'));
});
//Listen to the display namespace
displaySocket.on('connection', function (socket) {
    socket.on('playerColor', function (result) {
        clientSocket.to(result.id).emit('playerColor', result.color);
    });
    // log discconect to server
    socket.on('disconnect', function () {
    });
    // reload player controllers
    socket.on('reload', function () {
        clientSocket.emit('reload', null);
        // adds user to the connected user list, will be used for display syncing
    });
    socket.on('completeGame', function (scoreArray) {
        var index = 1;
        scoreArray.forEach(function(result){
            clientSocket.to(result.player.socketId).emit('completeGame', {score: result.score, color: result.player.colour, position: index});
            index++;
        });
    });
    socket.on('restartGame', function (scoreArray) {
        scoreArray.forEach(function(result){
            clientSocket.to(result.player.socketId).emit('restartGame', -1);
        });
    });

    socket.on('showPosition', function (result) {
        console.log("showPosition");
        console.log(result);
        clientSocket.to(result.id).emit('showPosition',result);
    });

    socket.on('restartController',function(result){
        clientSocket.to(result).emit('restartController');
    });

});
// Connection Event
clientSocket.on('connection', function (socket) {
    socket.on('changeDirection', function (direction) {
        displaySocket.emit('changeDirection', {direction: direction, id: socket.id});
    });
    //when reload is called emit add user again
    socket.on('reload', function () {
        displaySocket.emit('addUser', socket.id);
    });
    // register users back to the display
    socket.on('registerUser', function () {
        displaySocket.emit('addUser', socket.id)
    });
    // remove players from the display userlist and from the connected user list on the server
    socket.on('disconnect', function () {
        displaySocket.emit('removeUser', socket.id);
    });

    socket.on('showMap', function () {
        console.log('show map');
        displaySocket.emit('showMap', socket.id);
    });



});
//listen to commands
http.listen(3000, function () {
    console.log('listening on *:3000');
});
