function checkScore(player) {
    var inlist;
    for (i = 0; i < highscoreArray.length; i++) {
        if (highscoreArray[i].socketId == player.socketId) {
            inlist = true;
            if (inlist) {
                return;
            }
        }
    }
    for (i = 0; i < highscoreArray.length; i++) {
        if (compareScore(player, highscoreArray[i]) == -1) {
            highscoreArray[i] = player;
            return;
        }
    }
}

function compareScore(playerOne, PlayerTwo) {
    if (playerOne.score > PlayerTwo.score)
        return -1;
    if (playerOne.score < PlayerTwo.score)
        return 1;
    return 0;
}

function addToHighscore(player) {
    if (highscoreArray.length == 0) {
        highscoreArray.push(player);
    } else if (highscoreArray.length < 3) {
        for (i = 0; i < highscoreArray.length; i++) {
            if (highscoreArray[i].socketId == player.socketId) {
                return;
            }
            if (i == 3) {
                return;
            }
        }
        highscoreArray.push(player);
    }

}

// function to get score based on screenwidth and size of the squares
function getScore() {
    var scoreArray = [];
    for (var i = 0; i < Math.floor(screenWidth / squareSize); i++) {
        for (var j = 0; j < Math.floor(screenHeight / squareSize); j++) {
            if (playField[i][j].player) {
                var player = playField[i][j].player;


                if (player && $.grep(scoreArray, function (e) {
                        return e.player.socketId == player.socketId;
                    }) == 0) {
                    scoreArray.push({player: player, score: 0});
                }

                if (player != null) {
                    result = $.grep(scoreArray, function (e) {
                        return e.player.socketId == player.socketId;
                    });
                    updateScore(player.socketId, scoreArray);
                }
            }
        }
    }
    return scoreArray.sort(compareHighscore);
}

// builds update score for players based
function updateScore(socketId, scoreArray) {
    for (var i in scoreArray) {
        if (scoreArray[i].player.socketId == socketId) {
            scoreArray[i].score += 1;
            break;
        }
    }
}

//display score
function displayScore(scoreArray) {
    var index = 1;
    scoreArray.forEach(function (score) {
        drawHighScore(index, score.score.toString(), score.player.colour);
        index++;
    });
}

//compare to get highscores of players
function compareHighscore(a, b) {
    if (a.score > b.score)
        return -1;
    else if (a.score < b.score)
        return 1;
    else
        return 0;
}