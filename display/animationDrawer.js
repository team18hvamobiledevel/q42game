//holds the position of the bomb animations
var animationHolder = function () {
    this.bombExplosion = [
        [0,0],
        //first ring
        [0, 1], [1, 1], [-1, 1], [1, 0], [-1, 0], [1, -1], [0, -1], [-1, -1],
        //second ring
        [-2, 2], [-1, 2], [0, 2], [1, 2], [2, 2], [-2, -2], [-1, -2], [0, -2], [1, -2], [2, -2], [-2, -1], [-2, 0], [-2, 1], [2, 1], [2, 0], [2, -1],
        //3rd ring
        [-1, 3], [0, 3], [1, 3], [-1, -3], [0, -3], [1, -3], [-3, 1], [-3, 0], [-3, -1], [3, 1], [3, 0], [3, -1]
    ];
    this.explodeArray = [
        // outer explosion
        [-2, 0], [-1, -1], [0, -2], [1, -1], [2, 0], [1, 1], [-1, 1],
        // inner explosion
        [-1, 0], [0, 0], [0, -1], [0, 1], [1, 0], [0, 2]
    ];
};
//draws bomb explosion.
function drawBombAnimation(arrayToDraw, x, y, color) {

    var origin = playField[x][y];

    for (var i = 0; i < arrayToDraw.length; i++) {
        if (x + arrayToDraw[i][0] > widthBlocks - 1) {
            x = x - widthBlocks;
        }
        else if (x + arrayToDraw[i][0] < 0) {
            x = widthBlocks + x;
        }
        if (y + arrayToDraw[i][1] > heightBlocks - 1) {

            y = y - heightBlocks;

        }
        else if (y + arrayToDraw[i][1] < 0) {

            y = heightBlocks + y;

        }

        var column = x + arrayToDraw[i][0],
            row = y + arrayToDraw[i][1],
            sq = playField[column][row];
        if(!sq.isBomb && !sq.isShootingPowerUp) {

            if (sq.player != null && sq.standingOn && sq.player != origin.bombOwnedBy && sq.player.direction != -1 && !sq.player.isInvincible) {

                this.respawnPlayer(sq.player);
            }

            checkSquareCounter(sq, origin);

            drawSquare(sq, color);

            if (origin.bombOwnedBy != null) {
                checkTile(column, row, origin.bombOwnedBy);
                sq.player = origin.bombOwnedBy;
            } else {
                checkTile(column, row, null);
                sq.player = null;
            }
        }
    }
}

function checkSquareCounter(sq, origin){
    if(sq.player == null && origin.bombOwnedBy != null){
        squareCounter--;
    }else if (sq.player != null && origin.bombOwnedBy == null){
        squareCounter++;
    }
}
