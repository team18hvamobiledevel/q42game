var Square = function (posX, posY, player, standingOn) {
    this.posX = posX;
    this.posY = posY;
    this.player = player;
    this.standingOn = standingOn;
    this.isBomb = false;
    this.isBigger = false;
    this.isInvincible = false;
    this.bombOwnedBy = null;
    this.bombTimer = 1250;
    this.biggerTimer = 750;
    this.invincibleTimer = 750;
    this.ticks = 0;
    this.invincible = false;
    this.flicker = false;
    this.flickerTicks = 0;
    this.flickerTimer = 40;
    this.pickedUpBigger = false;
    this.pickedUpInvincible = false;

    //bomb variables
    this.canPickUp = false;
    this.bombNotPlaced = true;

    //shooting variables
    this.isShootingPowerUp = false;

    this.hasFlicker = function () {
        this.flicker = !this.flicker;
        this.flickerTimer -= 1.5;
        return this.flicker;
    };
    this.hasInvincible = function () {
        this.invincible = !this.invincible;
        this.flickerTimer -= 1.5;
        return this.invincible;
    };

};
