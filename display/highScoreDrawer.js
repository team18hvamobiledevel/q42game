//holds the position of numbers and text
var highScoreHolder = function () {
    this.highScore = [
        //H
        [0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [1, 2], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4],
        //I
        [4, 0], [4, 2], [4, 3], [4, 4],
        //G
        [6, 1], [6, 2], [6, 3], [7, 0], [7, 4], [8, 0], [8, 2], [8, 4], [9, 2], [9, 3],
        //H
        [11, 0], [11, 1], [11, 2], [11, 3], [11, 4], [12, 2], [13, 0], [13, 1], [13, 2], [13, 3], [13, 4],
        //S
        [15, 0], [15, 1], [15, 2], [15, 4], [16, 0], [16, 2], [16, 4], [17, 0], [17, 2], [17, 3], [17, 4],
        //C
        [19, 0], [19, 1], [19, 2], [19, 3], [19, 4], [20, 0], [20, 4], [21, 0], [21, 4],
        //O
        [23, 0], [23, 1], [23, 2], [23, 3], [23, 4], [24, 0], [24, 4], [25, 0], [25, 1], [25, 2], [25, 3], [25, 4],
        //R
        [27, 0], [27, 1], [27, 2], [27, 3], [27, 4], [28, 0], [28, 2], [29, 0], [29, 1], [29, 3], [29, 4],
        //E
        [31, 0], [31, 1], [31, 2], [31, 3], [31, 4], [32, 0], [32, 2], [32, 4], [33, 0], [33, 4]
    ];

    this.one = [
        [0, 1], [1, 0], [1, 1], [1, 2], [1, 3], [0, 4], [1, 4], [2, 4]
    ];

    this.two = [
        [0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [1, 2], [0, 2], [0, 3], [0, 4], [1, 4], [2, 4]
    ];

    this.three = [
        [0, 0], [1, 0], [2, 0], [0, 4], [1, 4], [2, 4], [2, 1], [1, 2], [2, 3]
    ];

    this.four = [
        [0, 0], [2, 0], [0, 1], [2, 1], [0, 2], [1, 2], [2, 2], [2, 3], [2, 4]
    ];

    this.five = [
        [0, 0], [1, 0], [2, 0], [0, 4], [1, 4], [2, 4], [0, 1], [0, 2], [1, 2], [2, 2], [2, 3]
    ];

    this.six = [
        [0, 0], [1, 0], [2, 0], [0, 4], [1, 4], [2, 4], [0, 1], [0, 2], [1, 2], [2, 2], [0, 3], [2, 3]
    ];

    this.seven = [
        [0, 0], [1, 0], [2, 0], [2, 1], [1, 2], [0, 3], [0, 4]
    ];

    this.eight = [
        [0, 0], [1, 0], [2, 0], [0, 4], [1, 4], [2, 4], [0, 1], [2, 1], [0, 2], [1, 2], [2, 2], [0, 3], [2, 3]
    ];

    this.nine = [
        [0, 0], [1, 0], [2, 0], [0, 4], [1, 4], [2, 4], [0, 1], [2, 1], [0, 2], [1, 2], [2, 2], [2, 3]
    ];

    this.zero = [
        [0, 0], [1, 0], [2, 0], [0, 4], [1, 4], [2, 4], [0, 1], [2, 1], [0, 2], [2, 2], [0, 3], [2, 3]
    ];
};
//draws the score
function drawScores(highScoreField, arrayToDraw, x, y, color) {
    for (var i = 0; i < arrayToDraw.length; i++) {
        var sq = highScoreField[x + arrayToDraw[i][0]][y + arrayToDraw[i][1]];
        drawSquare(sq, color);
    }
}
//draws HIGHSCORE text on screen after game
function drawHighscoreText(highScoreField, color, x, y, arrayToDraw) {
    for (var i = 0; i < arrayToDraw.length; i++) {
        var sq = highScoreField[x + arrayToDraw[i][0]][y + arrayToDraw[i][1]];
        drawSquare(sq, color);
    }
}
//call the draw function for scores with parameters
function drawScore(highScoreField, score, color, index) {
    var highscoreD = new highScoreHolder();
    var sc;

    for (var i = score.length; i > 0; i--) {
        sc = score[i - 1];
        if (sc == 0) {
            drawScores(highScoreField, highscoreD.zero, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 1) {
            drawScores(highScoreField, highscoreD.one, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 2) {
            drawScores(highScoreField, highscoreD.two, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 3) {
            drawScores(highScoreField, highscoreD.three, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 4) {
            drawScores(highScoreField, highscoreD.four, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 5) {
            //drawFive(numberPosArray[index].x, numberPosArray[index].y, color);
            drawScores(highScoreField, highscoreD.five, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 6) {
            drawScores(highScoreField, highscoreD.six, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 7) {
            drawScores(highScoreField, highscoreD.seven, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 8) {
            drawScores(highScoreField, highscoreD.eight, numberPosArray[index].x, numberPosArray[index].y, color);
            index -= 1;
        }
        if (sc == 9) {
            drawScores(highScoreField, highscoreD.nine, numberPosArray[index].x, numberPosArray[index].y, color)
            index -= 1;
        }
        if (sc == null) {
            return;
        }
    }
}
