function r2d(x) {
    return x / (Math.PI / 180);
}

function d2r(x) {
    return x * (Math.PI / 180);
}

var particles = [],
    arr = [];

function addParticles(particle_count, squareSize, player, colour, x, y, scoreBar) {


    angleBoundaries = getAngle(player);

    angle = angleBoundaries.min;


    if (player === undefined) {
        var startX = x * squareSize + (squareSize / 2),
            startY = y * squareSize + (squareSize / 2) + scoreBar;

    } else {
        var startX = player.x * squareSize + (squareSize / 2),
            startY = player.y * squareSize + (squareSize / 2) + scoreBar;
    }


    offset_x = $("#player_debris").width() / 2;
    offset_y = $("#player_debris").height() / 2;


    for (i = 0; i < particle_count; i++) {
        rad = d2r(angle);
        x = Math.cos(rad) * (80 + Math.random() * 20);
        y = Math.sin(rad) * (80 + Math.random() * 20);
        arr.push([startX + x, startY + y]);
        z = $('<div class="debris" />');


        if (player != undefined) {
            z.css({
                "left": startX - offset_x,
                "top": startY - offset_x,
                "background-color": player.colour
            }).appendTo($("#content"));
        } else {
            z.css({
                "left": startX - offset_x,
                "top": startY - offset_x,
                "background-color": colour
            }).appendTo($("#content"));
        }
        particles.push(z);

        if (player != undefined) {
            angle += 180 / particle_count;
        } else {
            angle += 360 / particle_count;
        }


    }


}

function pop() {
    $.each(particles, function (i, v) {

        if (arr[i][1] < screenHeight || arr[i][1] > screenHeight && arr[i][0] < screenWidth || arr[i][0] > screenWidth) {
            $(v).show();
            $(v).animate(
                {
                    top: arr[i][1],
                    left: arr[i][0],
                    width: 4,
                    height: 4,
                    opacity: 0
                }, 600, function () {
                    $(v).remove()
                });
        }
    });

    particles = [];
    arr = [];
}

function getAngle(player) {
    var angle = {
        min: 0,
        max: 0
    };

    if (!player) {
        angle.min = 0;
        angle.max = 360;
        return angle;
    }

    if (player.direction == 2) {
        angle.min = 180;
        angle.max = 360;
    } else if (player.direction == 3) {
        angle.min = 270;
        angle.max = 90;
    } else if (player.direction == 0) {
        angle.min = 360;
        angle.max = 180;
    } else if (player.direction = 1) {
        angle.min = 90;
        angle.max = 270;
    }
    return angle;

}

function gunPop(color, fromX, fromY, toX, toY, direction) {
    if(direction == 0 || direction == 2){
        direction = "vertical";
    }else{
        direction = "horizontal";
    }

    if(direction==0){
        toY -=40;
    }else if(direction==1){
        toX += 40;
    }else if(direction==2){
        toY += 40;
    }else if(direction==3){
        toX -= 40;
    }


    z = $('<div class="debris-small-'+direction+'"/>');
    z.css({
        "left": fromX+10,
        "top": fromY+10,
        "background-color": color
    }).appendTo($("#content"));
    particles.push(z);

    $.each(particles, function (i, v) {
        $(v).show();
        
        $(v).animate(
            {
                top: toY,
                left: toX
            }, 300, function () {
                $(v).remove()
            });
    });

    particles = [];
    arr = [];
}
