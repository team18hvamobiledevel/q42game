function spawnGun() {

    var rnd = (Math.random() * 100);
    var pickRNG = (Math.random() * 100);
    if (rnd >= 0.3 && rnd <= 100) {
        return;
    }

    if(POWERUPS_SPAWNED >= MAX_POWERUPS){
        return;
    }

    var x = Math.floor((Math.random() * Math.floor(screenWidth / squareSize))),
        y = Math.floor((Math.random() * Math.floor(screenHeight / squareSize)));

    var square = playField[x][y];

    if (!square) {
        return;
    }

    POWERUPS_SPAWNED++;
    this.drawGun(square, "#000");
    square.isShootingPowerUp = true;

    shootingArray.push(square);
}

function redrawPowerUps() {
    var index = 0;

    bombArray.forEach(function (square) {
        if(square.canPickUp) {
            this.drawPickUpBomb(square, "#000");
        }
        index++;
    });
}
