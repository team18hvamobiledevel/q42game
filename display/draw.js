// draws the sqaures for player objects and tails
function drawCollisionSplat(player, playerTwo, arrayToDraw, x, y) {
    for (var i = 0; i < arrayToDraw.length; i++) {
        if (x + arrayToDraw[i][0] > widthBlocks - 1) {
            x = x - widthBlocks;
        } else if (x + arrayToDraw[i][0] < 0) {
            x = widthBlocks + x;
        }
        if (y + arrayToDraw[i][1] > heightBlocks - 1) {
            y = y - heightBlocks;
        }
        else if (y + arrayToDraw[i][1] < 0) {
            y = heightBlocks + y;
        }

        var square = playField[x + arrayToDraw[i][0]][y + arrayToDraw[i][1]];

        if (square.player === null) {
            squareCounter--;
        }

        if (i < 6) {
            checkTile(x + arrayToDraw[i][0], y + arrayToDraw[i][1], player);
            this.drawSquare(square, player.colourLight);
            square.player = player;
        } else {
            checkTile(x + arrayToDraw[i][0], y + arrayToDraw[i][1], playerTwo);
            this.drawSquare(square, playerTwo.colourLight);
            square.player = playerTwo;
        }
    }
}

// draws the squares for player objects and tails
function drawPlayer(colour, player) {
    var square = playField[player.x][player.y];

    if (square.player === null) {
        squareCounter--;
    }

    square.player = player;

    this.drawSquare(square, colour);

    if(square.player.ticksInvincible > INVINCIBLE_TIME){
      square.player.isInvincible = false;
      invincibleSpawned = false;
    }

    if(square.player != null && square.player.isBigger && square.player.ticksLoop < BIGGER_TIME){

      var squareTwo = playField[player.x+1][player.y]
      , squareThree = playField[player.x][player.y+1]
      , squareFour = playField[player.x+1][player.y+1];

            checkTile(squareTwo.posX/squareSize, squareTwo.posY/squareSize, player);
            checkTile(squareThree.posX/squareSize, squareThree.posY/squareSize, player);
            checkTile(squareFour.posX/squareSize, squareFour.posY/squareSize, player);

      if(squareTwo.player === null){
          squareCounter--;
      }
      if(squareThree.player === null){
          squareCounter--;
      }
      if(squareFour.player === null){
          squareCounter--;
      }

      squareTwo.player = player;
      squareThree.player = player;
      squareFour.player = player;

      this.drawSquare(squareTwo, colour, player);
      this.drawSquare(squareThree, colour, player);
      this.drawSquare(squareFour, colour, player);
    } else {
      square.player.isBigger = false;
      biggerSpawned = false;
    }

}

function drawSquare(square, colour) {
    ctx.beginPath();
    ctx.rect(square.posX + 1, square.posY + 1, squareSize - 1, squareSize - 1);
    ctx.fillStyle = colour;
    ctx.fill();
    ctx.closePath();
}
//drawCircle
function drawCircle(square, colour) {
    ctx.beginPath();

    ctx.arc(square.posX + 10, square.posY + 10, (squareSize / 2) - 1, 0, 2 * Math.PI);
    ctx.fillStyle = colour;
    ctx.fill();
    ctx.closePath();
}

// draw Triangle
function drawTriangle(square, colour) {
  ctx.beginPath();
  ctx.moveTo(square.posX+1, square.posY+1);
  ctx.lineTo(square.posX+1, square.posY + squareSize);
  ctx.lineTo(square.posX + squareSize, square.posY + squareSize);
  ctx.fillStyle = colour;
  ctx.fill();
  ctx.closePath();
}

// Draw highscore in blocks
function drawHighScore(place, score, colour) {
    var highScoreField = [];
    var highscoreD = new highScoreHolder();

    for (var i = 0; i < Math.floor(screenWidth / squareSize); i++) {
        var objects = [];
        for (var j = 0; j < Math.floor(screenHeight / squareSize); j++) {
            objects.push(new Square(i * squareSize, j * squareSize, null));
        }
        highScoreField.push(objects);
    }

    // calculate middle
    var totalWidth = Math.floor(screenWidth / squareSize);
    var leftOverWidth = Math.floor(totalWidth - 34);
    var leftLeftOver = Math.floor(leftOverWidth / 2);

    var totalHeight = Math.floor(screenHeight / squareSize);
    var leftOverHeight = Math.floor(totalHeight - 23);
    var leftUpperHeight = Math.floor(leftOverHeight / 2);

    var posX = 7, posY = 6;
    for (i = 0; i <= 11; i++) {
        numberPosArray[i] = {x: leftLeftOver + posX, y: leftUpperHeight + posY};
        if (posX == 19) {

            posY += 6;
            posX = 7;
            continue;
        }
        posX += 4;
    }
    if (place == 1) {
        drawHighscoreText(highScoreField, colour, leftLeftOver, leftUpperHeight, highscoreD.highScore);
        drawScore(highScoreField, score, colour, 3);
    }
    if (place == 2) {
        drawScore(highScoreField, score, colour, 7);
    }
    if (place == 3) {
        drawScore(highScoreField, score, colour, 11);
    }
}

//draws GUN
function drawGun(square, colour) {
    ctx.beginPath();
    ctx.rect(square.posX + 2, square.posY + 5, 17, 6);
    ctx.rect(square.posX + 2, square.posY + 3, 6, 15);
    ctx.rect(square.posX + 15, square.posY + 3, 3, 3);
    ctx.rect(square.posX + 12, square.posY + 11, 2, 4);
    ctx.rect(square.posX + 9, square.posY + 11, 2, 2);
    ctx.rect(square.posX + 6, square.posY + 14, 7, 2);
    ctx.fillStyle = colour;
    ctx.fill();
    ctx.closePath();
}

//draws bombs that you can pick up
function drawPickUpBomb(square, colour) {
    ctx.beginPath();
    ctx.arc(square.posX + 10.5, square.posY + 10.5, (squareSize / 2)-1 - 1, 0, 2 * Math.PI);
    ctx.lineWidth = 2;
    ctx.strokeStyle = colour;
    ctx.moveTo(square.posX+2, square.posY+2);
    ctx.lineTo(square.posX+squareSize-1,square.posY+squareSize-1);

    ctx.moveTo(square.posX+squareSize-2, square.posY+2);
    ctx.lineTo(square.posX+2,square.posY+squareSize-1);

    ctx.stroke();
    ctx.closePath();
}
