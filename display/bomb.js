function spawnBomb() {
    var rnd = (Math.random() * 100);
    var pickRNG = (Math.random() * 100);
    if (rnd >= BOMB_CHANCE && rnd <= 100) {
        return;
    }

    if(BOMBS_SPAWNED >= MAXIMUM_BOMBS){
        return;
    }

    var x = Math.floor((Math.random() * Math.floor(screenWidth / squareSize))),
        y = Math.floor((Math.random() * Math.floor(screenHeight / squareSize)));

    var square = playField[x][y];

    if (!square) {
        return;
    }
    square.isBomb = true;
    if (pickRNG <= 20) {
        square.canPickUp = true;
    }
    BOMBS_SPAWNED++;
    if(!square.canPickUp) {
        this.drawCircle(square, "#000");
    } else {
        this.drawPickUpBomb(square, "#000");
    }
    if(!square.canPickUp) {
        bombArray.push(square);
    }
}
//explodes the bomb when ready
function explodeBomb(square, index) {
    var color = "#FFFFFF"
        , colorAnimate = "#FF0000";

    //animationholder
    var animation = new animationHolder();

    if (square.bombOwnedBy != null) {
        color = square.bombOwnedBy.colourLight;

    }

    if (square.bombOwnedBy != null) {
        addParticles(6, squareSize, undefined, square.bombOwnedBy.colour, (square.posX / squareSize), (square.posY / squareSize), scoreBarHeight);
    } else {
        addParticles(6, squareSize, undefined, colorAnimate, (square.posX / squareSize), (square.posY / squareSize), scoreBarHeight);
    }

    pop();
    drawBombAnimation(animation.bombExplosion, (square.posX / squareSize), (square.posY / squareSize), color);

    square.isBomb = false;
    square.bombOwnedBy = null;
    drawSquare(square, color);
    bombArray.splice(index, 1);
}

function tickBombs() {
    var index = 0;

    bombArray.forEach(function (square) {
        //if(!square.bombNotPlaced) {
        //    return;
        //}
        bombFlicker(square);
        if (square.ticks > square.bombTimer) {
            //console.log("ayy " + square.bombOwnedBy);
            explodeBomb(square, index);
            BOMBS_SPAWNED--;
        }

        square.ticks++;
        index++;
    });

    spawnBomb();
}
//make the bomb flicker
function bombFlicker(square) {
    square.flickerTicks++;
    if (square.flickerTicks <= square.flickerTimer) {
        return;
    }
    square.hasFlicker();
    // If bomb is not owned
    if (square.bombOwnedBy == null) {
        if (square.flicker) {
            this.drawCircle(square, "#000");

        } else {
            this.drawCircle(square, "#FFFFFF");
        }
        square.flickerTicks = 0;
        return;
    }

    // If bomb is owned by a player
    if (square.flicker) {
        this.drawCircle(square, square.player.colourLight);
    } else {
        this.drawCircle(square, square.player.colour);
    }

    square.flickerTicks = 0;
}
