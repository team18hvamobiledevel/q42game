var canvas = document.getElementById("myCanvas"),
    ctx = canvas.getContext("2d");

// screen settings
var screenWidth = window.innerWidth
    , screenHeight = window.innerHeight * 0.85
    , squareSize = 20
    , widthBlocks = Math.floor(screenWidth / squareSize)
    , heightBlocks = Math.floor(screenHeight / squareSize)
    , scoreBarHeight = (window.innerHeight * 0.15);

// set screen to smallest size
if (screenWidth < 800) {
    screenWidth = 800;
}

//highscore array
var highscoreArray = [];

// hueCounter for hue
var hueCounter = 3; // hue

var counterDark = 1, gamePaused = false;
var colourHueArray = ["red", "orange", "yellow", "green", "blue", "purple", "pink"], darkArray = ["bright", "dark", "normal"];

//vars for game logic
var numberPosArray = [], playField = [], playerArray = [], bombArray = [], biggerArray = [], invincibleArray = [], shootingArray = [];

//amount of squares needed to be remaining to win, max bombs that will spawn, the chance that bombs will spawn
var MAX_WIN_SQUARE = 0,
    MAXIMUM_BOMBS = 3,
    MAXIMUM_INVINCIBLE = 2,
    MAXIMUM_BIGGER = 2,
    BOMB_CHANCE = 0.3,
    BIGGER_CHANCE = 0.3,
    INVINCIBLE_CHANCE = 0.2,
    BOMBS_SPAWNED = 0,
    BIGGER_TIME = 100,
	  RESTART_DELAY = 5000,
    INVINCIBLE_TIME = 60,
	  MAX_POWERUPS = 2,
    POWERUPS_SPAWNED = 0;

// counter for squares to win
var squareCounter;
// powerUps active
var invincibleSpawned = false,
biggerSpawned = false;
//init for screen
var initialized = false;

// Server socket code
var socket = io('/display');

// Add user to game
socket.on('addUser', function (socketId) {
    addPlayer(socketId);
});

//disconect player
socket.on('removeUser', function (socketId) {
    var player = playerArray[socketId];
    if(player.hasBomb){
        BOMBS_SPAWNED--;
    }
    removePlayer(player);
});

//change direction
socket.on('changeDirection', function (result) {
    if (result.direction <= 3 && result.direction != "elsewhere") {
        playerArray[result.id].direction = result.direction;
        if (playerArray[result.id].tap) {
            playerArray[result.id].tap = false;
        }

    } else if (result.direction === 4) {
        playerArray[result.id].tap = true;
    }
    else if (result.direction === 5) {
        playerArray[result.id].press = true;
    }
});

socket.on('showMap', function(result){


    if(playerArray[result].showMap){
        hideMap(result);
    }
    playerArray[result].showMap=!playerArray[result].showMap;
});

function hideMap(id){

    positions = {
        'hide': true,
        'id': id
    };
    socket.emit('showPosition', positions);

}

//init
function init() {
    if (initialized) {
        return;
    }
    requestAnimationFrame(pop);

    playField = [];
    for (i = 0; i < Math.floor(screenWidth / squareSize); i++) {
        var objects = [];
        for (j = 0; j < Math.floor(screenHeight / squareSize); j++) {
            objects.push(new Square(i * squareSize, j * squareSize, null));
        }
        playField.push(objects);
    }

    drawGrid(screenWidth, screenHeight);
    squareCounter = Math.floor(screenWidth / squareSize) * Math.floor(screenHeight / squareSize);
    initialized = true;
}


// Game functions
function drawGrid(gridX, gridY) {

    if (gridX < 800) {
        gridX = 800;
    }

    var newX, newY;

    myCanvas.width = gridX;
    myCanvas.height = gridY;

    newY = Math.floor(gridY / squareSize);
    gridY = newY * squareSize;

    newX = Math.floor(gridX / squareSize);
    gridX = newX * squareSize;
    // Vertical grid lines
    for (var x = 0.5; x < gridX + 1; x += squareSize) {
        ctx.moveTo(x, 0);
        ctx.lineTo(x, gridY);
    }

    // Horizontal grid lines
    for (var y = 0.5; y < gridY + 1; y += squareSize) {
        ctx.moveTo(0, y);
        ctx.lineTo(gridX, y);
    }

    ctx.strokeStyle = "#F2F2F2";

    ctx.stroke();
}

// removes player from the array and the game
function removePlayer(player) {
    // Prevents errors when the display is reloaded.
    // While controllers are connected.
    if (player != null && playerArray.hasOwnProperty(player.socketId)) {
        playField[player.x][player.y].standingOn = false;
        drawPlayer(player.colourLight, player);
        player.disconnected = true;
        delete playerArray[player.socketId];
        highscoreArray.splice(player,1);
        delete highscoreArray[player];
    }
}

function respawnPlayer(player) {
    if (player != null && playerArray.hasOwnProperty(player.socketId)) {
        playField[player.x][player.y].standingOn = false;
        drawPlayer(player.colourLight, player);
        var y = Math.floor((Math.random() * Math.floor(screenHeight / squareSize))),
            x = Math.floor((Math.random() * Math.floor(screenWidth / squareSize)));
        player.isBigger = false;
        player.isInvincible = false;
        player.isShooting = false;
        player.direction = -1;
        player.y = y;
        player.x = x;
        checkTile(x, y, player);
        var sq = playField[x][y];

        if (sq.player == null) {
            squareCounter--;
        } else {
            sq.player.score--;
        }

        sq.standingOn = true;
        sq.player = player;
    }

    showMap(player);
    socket.emit('restartController',player.socketId);
}

// creates a player object and adds it to the gamefield
function addPlayer(socketId, colour, colourLight, score) {
    var y = Math.floor((Math.random() * Math.floor(screenHeight / squareSize))),
        x = Math.floor((Math.random() * Math.floor(screenWidth / squareSize))),
        player = null;

    if (colour !== undefined && colourLight !== undefined) {
        player = new Player(x, y, -1, colour, colourLight);
    } else {
        var myColour = getRandomColorHue(colourHueArray[hueCounter], darkArray[counterDark]),
            myColourLight = getLighterColor(myColour, 25);
        player = new Player(x, y, -1, myColour, myColourLight);
    }

    // Register player in array
    player.socketId = socketId;
    playerArray[socketId] = player;

    // Send color to controller
    sendColorToController(socketId, player.colour);

    // Saturation control
    hueCounter += 1;
    if (hueCounter == 7) {
        hueCounter = 0;
        counterDark += 1;
    }
    if (counterDark == 3) {
        counterDark = 0;
    }

    player.socketId = socketId;
    playerArray[socketId] = player;

    if (score) {
        player.score = score;
    }
    checkTile(x, y, player);
    var sq = playField[x][y];

    if (sq.player == null) {
        squareCounter--;
    } else {
        sq.player.score--;
    }

    sq.standingOn = true;
    sq.player = player;
}
// updates player collour to the controller
function sendColorToController(socketId, color) {
    var playerColor = {
        id: socketId,
        color: color

    };
    socket.emit('playerColor', playerColor);
}

function checkTile(x, y, player) {
    var square = playField[x][y];

    // EMPTY PLAYER, REMOVE SCORE
    if (player == null) {
        if (square.player != null && !(square.player.direction == -1 && square.standingOn)) {
            square.player.score--;
            displayScoreBar();
        }
        return;
    }

    //PLAYER NOT AVAILABLE, ADD SCORE
    if (!square.player) {
        player.score++;
    } else if (square.player != player && typeof square.player != 'undefined' && !(square.player.direction == -1 && square.standingOn)) {
        //PLAYER AVAILABLE, NOT FLICKERING, NOT UNDEFINED, ADD SCORE AND REMOVE SCORE
        square.player.score--;
        player.score++;
    }

    //PLAYER IS NOT NULL, ADD TO SCORE BAR.
    if (player != null) {
        if (highscoreArray.length < 3) {
            addToHighscore(player);
        }

        highscoreArray.sort(compareScore);
        checkScore(player);
    }
    $("#displayScore").show();

    displayScoreBar();
}

function hasBiggerCollision(player) {

    var square = playField[player.x][player.y],
        squareTwo = playField[player.x + 1][player.y],
        squareThree = playField[player.x][player.y + 1],
        squareFour = playField[player.x + 1][player.y + 1],
        playerTwo = square.player;

    if (squareTwo.player && squareTwo.player.socketId != player.socketId) {
        playerTwo = squareTwo.player;
    }
    else if (squareThree.player && squareThree.player.socketId != player.socketId) {
        playerTwo = squareThree.player;
    }
    else if (squareFour.player && squareFour.player.socketId != player.socketId) {
        playerTwo = squareFour.player;
    }
    //animationholder
    var animation = new animationHolder();

    //is bomb collision
    if (square.isBomb) {
        if (square.isBomb) {
            if (square.bombNotPlaced) {
                if (square.canPickUp && square.bombNotPlaced) {
                    square.isBomb = false;
                    square.bombOwnedBy = null;

                    player.hasBomb = true;
                } else {

                    square.bombOwnedBy = player;
                }
            } else {
                if (!square.bombNotPlaced && square.bombOwnedBy != player) {
                    square.bombTimer = 0;
                }
            }
        }
    }
    if (squareTwo.isBomb != undefined && squareTwo.isBomb) {
        if (squareTwo.isBomb) {
            if (squareTwo.bombNotPlaced) {
                if (squareTwo.canPickUp && squareTwo.bombNotPlaced) {
                    squareTwo.isBomb = false;
                    squareTwo.bombOwnedBy = null;

                    player.hasBomb = true;
                } else {
                    squareTwo.bombOwnedBy = player;
                }
            } else {
                if (!squareTwo.bombNotPlaced && squareTwo.bombOwnedBy != player) {
                    squareTwo.bombTimer = 0;
                }
            }
        }
    }
    if (squareThree.isBomb != undefined && squareThree.isBomb) {
        if (squareThree.isBomb) {
            if (squareThree.bombNotPlaced) {
                if (squareThree.canPickUp && squareThree.bombNotPlaced) {
                    squareThree.isBomb = false;
                    squareThree.bombOwnedBy = null;

                    player.hasBomb = true;
                } else {
                    squareThree.bombOwnedBy = player;
                }
            } else {
                if (!squareThree.bombNotPlaced && squareThree.bombOwnedBy != player) {
                    squareThree.bombTimer = 0;
                }
            }
        }
    }
    if (squareFour.isBomb != undefined && squareFour.isBomb) {
        if (squareFour.isBomb) {
            if (squareFour.bombNotPlaced) {
                if (squareFour.canPickUp && squareFour.bombNotPlaced) {
                    squareFour.isBomb = false;
                    squareFour.bombOwnedBy = null;

                    player.hasBomb = true;
                } else {
                    squareFour.bombOwnedBy = player;
                }
            } else {
                if (!squareFour.bombNotPlaced && squareFour.bombOwnedBy != player) {
                    squareFour.bombTimer = 0;
                }
            }
        }
    }
    // is bigger collision
    if (square.isBigger || squareTwo.isBigger || squareThree.isBigger || squareFour.isBigger) {
        biggerSpawned = true;
        player.isBigger = true;
        player.ticksLoop = 0;
        if (square.isBigger) {
            square.pickedUpBigger = true;
            square.bombOwnedBy = player;
        } else if (squareTwo.isBigger) {
            squareTwo.pickedUpBigger = true;
            squareTwo.bombOwnedBy = player;
        } else if (squareThree.isBigger) {
            squareThree.pickedUpBigger = true;
            squareThree.bombOwnedBy = player;
        } else if (squareFour.isBigger) {
            squareFour.pickedUpBigger = true;
            squareFour.bombOwnedBy = player;
        }
        return false;
    }
    // is invincible collision
    if (square.isInvincible || squareTwo.isInvincible || squareThree.isInvincible || squareFour.isInvincible) {
        invincibleSpawned = true;
        player.isInvincible = true;
        player.ticksInvincible = 0;

        if (square.isInvincible) {
            square.pickedUpInvincible = true;
            square.bombOwnedBy = player;
        } else if (squareTwo.isInvincible) {
            squareTwo.pickedUpInvincible = true;
            squareTwo.bombOwnedBy = player;
        } else if (squareThree.isInvincible) {
            squareThree.pickedUpInvincible = true;
            squareThree.bombOwnedBy = player;
        } else if (squareFour.isInvincible) {
            squareFour.pickedUpInvincible = true;
            squareFour.bombOwnedBy = player;
        }
        return false;
    }

    //if is shooting powerup
    if (square.isShootingPowerUp || squareTwo.isShootingPowerUp || squareThree.isShootingPowerUp || squareFour.isShootingPowerUp) {
        if (player.isShooting) {

            player.shootTicks = 0;
            POWERUPS_SPAWNED--;
        }
        player.isShooting = true;
        square.isShootingPowerUp = false;
        squareTwo.isShootingPowerUp = false;
        squareThree.isShootingPowerUp = false;
        squareFour.isShootingPowerUp = false;
    }

    // If player two is not defined: EMPTY BLOCK
    if (!playerTwo) {
        player.speed = 10;
        return;
    }
    // If player two is defined and id matches player one: YOUR BLOCK
    if (playerTwo && player.socketId == playerTwo.socketId &&
        (!squareTwo.player || squareTwo.player.socketId == player.socketId) &&
        (!squareThree.player || squareThree.player.socketId == player.socketId) &&
        (!squareFour.player || squareFour.player.socketId == player.socketId)) {
        player.speed = 10;
        return;
    }
    // If playerTwo is defined, it is not us, and someone is standing on it
    // SOME ELSE HIS/HER BLOCK!
    if (playerTwo && square.standingOn &&
        playerTwo.direction != -1 ||
        playerTwo && squareTwo.standingOn &&
        playerTwo.direction != -1 ||
        playerTwo && squareThree.standingOn &&
        playerTwo.direction != -1 ||
        playerTwo && squareFour.standingOn &&
        playerTwo.direction != -1) {
        addParticles(4, squareSize, player, undefined, undefined, undefined, scoreBarHeight);
        addParticles(4, squareSize, playerTwo, undefined, undefined, undefined, scoreBarHeight);
        pop();

        this.drawCollisionSplat(player, playerTwo, animation.explodeArray, player.x, player.y);

        if (!player.isInvincible) {
            this.respawnPlayer(player);
        }
        if (!playerTwo.isInvincible) {
            this.respawnPlayer(playerTwo);
        }

        return true;
    }

      if (playerTwo) {
        if(player.isInvincible){
          player.speed = 9;
        } else {
        player.speed = 14;
      }
      }
}
// checks Collision
function hasCollision(player) {
    var square = playField[player.x][player.y],
        playerTwo = square.player;

        if(player.isBigger){
          return hasBiggerCollision(player);
        }

    //animationholder
    var animation = new animationHolder();

    //is bomb collision
    if (square.isBomb) {
        if (square.bombNotPlaced) {
            if (square.canPickUp && square.bombNotPlaced) {
                //logica voor pickup bombs
                square.isBomb = false;
                square.bombOwnedBy = null;

                player.hasBomb = true;
            } else {
                square.bombOwnedBy = player;
            }
        } else {
            if(!square.bombNotPlaced && square.bombOwnedBy != player) {
                square.bombTimer = 0;
                this.respawnPlayer(player);
            }
        }
    }

    // is bigger collision
    if(square.isBigger){
      biggerSpawned = true;
      square.bombOwnedBy = player;
      player.isBigger = true;
      player.ticksLoop = 0;
      square.pickedUpBigger = true;

      return false;
    }
    // is invincible collision
    if(square.isInvincible){
      invincibleSpawned = true;
      square.bombOwnedBy = player;
      player.isInvincible = true;
      player.ticksInvincible = 0;
      square.pickedUpInvincible = true;

      return false;
    }

    //if is shooting powerup
    if(square.isShootingPowerUp){
        if(player.isShooting){

            player.shootTicks = 0;
            POWERUPS_SPAWNED--;
        }
        player.isShooting = true;
        square.isShootingPowerUp = false;
    }


    // If player two is not defined: EMPTY BLOCK
    if (!playerTwo) {
        player.speed = 10;
        return;
    }

    // If player two is defined and id matches player one: YOUR BLOCK
    if (playerTwo && player.socketId == playerTwo.socketId) {
        player.speed = 10;
        return;
    }

    // If playerTwo is defined, it is not us, and someone is standing still on it
    // SOME ELSE HIS/HER MAIN BLOCK!
    if (playerTwo && square.standingOn && square.player.direction != -1) {
        if(player.hasBomb || playerTwo.hasBomb) {
            carrier = player;
            if(playerTwo.hasBomb){
                carrier = playerTwo;
            }
            square.bombOwnedBy = carrier;
            square.isBomb = true;
            square.bombNotPlaced = false;
            square.bombTimer = 0;
            bombArray.push(square);

            carrier.bombTicks = 0;
            carrier.hasBomb = false;
        }
        addParticles(4, squareSize, player, undefined, undefined, undefined, scoreBarHeight);
        addParticles(4, squareSize, playerTwo, undefined, undefined, undefined, scoreBarHeight);
        pop();

        this.drawCollisionSplat(player, playerTwo, animation.explodeArray, player.x, player.y);


        if(!player.isInvincible){
        this.respawnPlayer(player);
       }
        if(!playerTwo.isInvincible){
        this.respawnPlayer(playerTwo);
       }
        return true;
    }

    if (playerTwo) {
        if(player.isInvincible){
          player.speed = 9;
        } else {
        player.speed = 14;
      }
    }
}


function spawnBigger() {
    var rnd = (Math.random() * 100);
    if (rnd >= BIGGER_CHANCE && rnd <= 100) {
        return;
    }

    if(biggerArray.length >= MAXIMUM_BIGGER){
        return;
    }

    var x = Math.floor((Math.random() * Math.floor((screenWidth / squareSize)-1))) ,
    y = Math.floor((Math.random() * Math.floor((screenHeight / squareSize)-1)));

    var square = playField[x][y];

    if (!square) {
        return;
    }

    square.isBigger = true;

    this.drawTriangle(square, "#FFFFFF");
    biggerArray.push(square);
}

function removeBigger(square,index){
  var color = "#FFFFFF";

  if(square.player != null){
  //  square.player.score ++;
    color = square.player.colourLight;
  }

  square.isBigger = false;
  square.bombOwnedBy = null;
  drawSquare(square, color);
  biggerArray.splice(index, 1);
}

function tickBigger() {
    var index = 0;

    biggerArray.forEach(function (square) {
        biggerFlicker(square);
        if (square.ticks > square.biggerTimer || square.pickedUpBigger) {
        removeBigger(square,index);
        }

        square.ticks++;
        index++;
    });
    if(biggerSpawned == false){
    spawnBigger();
  }
}

function biggerFlicker(square) {
    square.flickerTicks++;
    if (square.flickerTicks <= square.flickerTimer) {
        return;
    }
    square.hasFlicker();
    // If bigger is not owned
    if (square.bombOwnedBy == null) {
        if (square.flicker) {
            this.drawTriangle(square, "#000");

        } else {
            this.drawTriangle(square, "#FFFFFF");
        }
        square.flickerTicks = 0;
        return;
    }

    // If bigger is owned by a player
    if (square.flicker) {
        this.drawTriangle(square, square.player.colourLight);
    } else {
        this.drawTriangle(square, square.player.colour);
    }

    square.flickerTicks = 0;
}

function spawnInvincible() {

    var rnd = (Math.random() * 100);
    if (rnd >= INVINCIBLE_CHANCE && rnd <= 100) {
        return;
    }

    if(invincibleArray.length >= MAXIMUM_INVINCIBLE){
        return;
    }

    var x = Math.floor((Math.random() * Math.floor(screenWidth / squareSize))),
        y = Math.floor((Math.random() * Math.floor(screenHeight / squareSize)));

    var square = playField[x][y];

    if (!square) {
        return;
    }

    square.isInvincible = true;

    this.drawTriangle(square, "#FFFFFF");
    invincibleArray.push(square);

}
function removeInvincible(square,index){
  var color = "#FFFFFF";

  if(square.player != null){
    //square.player.score ++;
    color = square.player.colourLight;
  }

  square.isInvincible = false;
  square.bombOwnedBy = null;
  drawSquare(square, color);
  invincibleArray.splice(index, 1);
}
function tickInvincible() {
    var index = 0;

    invincibleArray.forEach(function (square) {
        invincibleFlicker(square);
        if (square.ticks > square.invincibleTimer || square.pickedUpInvincible) {
          removeInvincible(square,index);
        }
        square.ticks++;
        index++;
    });
    if(invincibleSpawned == false){
    spawnInvincible();
  }
}

function invincibleFlicker(square) {
    square.flickerTicks++;
    if (square.flickerTicks <= square.flickerTimer) {
        return;
    }
    square.hasFlicker();
    // If bomb is not owned
    if (square.bombOwnedBy == null) {
        if (square.flicker) {
            this.drawTriangle(square, "#FF0000");

        } else {
            this.drawTriangle(square, "#FFFFFF");
        }
        square.flickerTicks = 0;
        return;
    }

    // If bomb is owned by a player
    if (square.flicker) {
        this.drawTriangle(square, square.player.colourLight);
    } else {
        this.drawTriangle(square, square.player.colour);
    }

    square.flickerTicks = 0;
}

// moves player in the gamefield. emits it to the server to sync between displays
function movePlayers() {
    for (var id in playerArray) {
        if (playerArray.hasOwnProperty(id)) {
            // Get player from array
            var player = playerArray[id];
            var square = playField[player.x][player.y];
            // If player is not moving flicker
            if (player.disconnected == false) {
                if (player.direction == -1) {
                    if (player.hasFlicker()) {
                        this.drawPlayer(player.colour, player);
                    } else {
                        this.drawPlayer(player.colourLight, player);
                    }
                    continue;
                }
            }


            if(player.hasBomb){
                if(player.bomFlicker()) {
                    this.drawPickUpBomb(square, "#000");
                } else {
                    this.drawPickUpBomb(square, player.colourLight);
                }
                //continue;
            }

            // Move player forward into his/her direction
            if (player.ticks == player.speed) {
                this.drawPlayer(player.colourLight, player);
                playField[player.x][player.y].standingOn = false;
                player.move(player);

                if (hasCollision(player)) {
                    return;
                }

                checkTile(player.x, player.y, player);
                playField[player.x][player.y].standingOn = true;
                drawPlayer(player.colour, player);
                showMap(player);
            }


            if(player.isInvincible){
              player.speed = 9;
            if (player.hasInvincible()) {
                this.drawPlayer(player.colour, player);
            } else {
                this.drawPlayer(player.colourLight, player);
            }
            //continue;
            }

            if(player.hasBomb) {
                player.bombTicks++;
                player.dropBomb(player);
            }

            player.ticks++;

        }
    }
}

function showMap(player){

    if(player.showMap){

       player.mapCount+=1;
        var x= playerArray[player.socketId].x,
            y= playerArray[player.socketId].y;

        x= (((x*squareSize)/screenWidth)*100);
        y=(((y*squareSize)/screenHeight)*100);

        positions = {
            'x': x,
            'y': y,
            'id': player.socketId
        };

        player.mapCount++;
        if(player.mapCount>=5){
            player.mapCount=0;
            socket.emit('showPosition', positions);

        }
    }
}

function clearGameState() {
    if (squareCounter <= MAX_WIN_SQUARE) {
        var scoreArray = getScore();
        socket.emit('completeGame', scoreArray);


        this.gamePaused = true;

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        drawGrid(screenWidth, screenHeight);
        displayScore(scoreArray);

        for (var id in playerArray) {
            if (playerArray.hasOwnProperty(id)) {
                playerArray[id].score = 0;
                respawnPlayer(playerArray[id]);
            }
        }

        setTimeout(function () {
            this.gamePaused = false;
            this.playField = [];
            this.initialized = false;
            socket.emit('restartGame', scoreArray);
        }, RESTART_DELAY);
    }
}

// Draw function that gets called in the update loop
function draw() {
    if (!gamePaused) {
        init();
        movePlayers();
        clearGameState();
        displayRemainingSquares();
        tickBombs();
        tickBigger();
        tickInvincible();
        spawnGun();
        //redrawPowerUps();

    }
}

// Game background
canvas.style.backgroundColor = "white";

//resize the canvas
window.onresize = function () {
    document.getElementById('myCanvas').setAttribute("height", Math.round(window.innerHeight * 0.85) + "px");
    document.getElementById('myCanvas').setAttribute("width", window.innerWidth + "px");
    screenHeight = Math.round(window.innerHeight * 0.85);
    screenWidth = window.innerWidth;
    scoreBarHeight = (window.innerHeight * 0.15);
    init();
};

$(document).ready(function () {

    screenHeight = window.innerHeight * 0.85;
    screenWidth = window.innerWidth;
    scoreBarHeight = window.innerHeight * 0.15;
    //Start gameloop
    setInterval(draw, 10);

    var showURL= document.getElementById('displayURL');
    var url=window.location.href.replace("/display", "");

    showURL.innerHTML=(url);
    $("#displayScore").hide();
    displayScoreBar();

});

jQuery.event.add(window, "resize", resizeFrame);

function resizeFrame() {
    var h = $(window).height();
    var w = $(window).width();

    for (i = 0; i < bombArray.length; i++) {

        bombArray.splice(i, 1);

    }
    if (h < 800 && w < 600) {
        widthBlocks = Math.floor(screenWidth / squareSize);
        heightBlocks = Math.floor(screenHeight / squareSize);
        for (var id in playerArray) {
            if (playerArray.hasOwnProperty(id)) {
                playerArray[id].score = 0;
                respawnPlayer(playerArray[id]);
            }
        }
        window.resizeTo(600, 800);
        init();
        displayScoreBar();
    }
    widthBlocks = Math.floor(screenWidth / squareSize);
    heightBlocks = Math.floor(screenHeight / squareSize);
    for (var id in playerArray) {
        if (playerArray.hasOwnProperty(id)) {
            playerArray[id].score = 0;
            respawnPlayer(playerArray[id]);
        }
    }

    initialized = false;
    init();
    displayScoreBar();
}
