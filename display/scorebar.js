function displayScoreBar() {

    var place = [document.getElementById('score-1'), document.getElementById('score-2'), document.getElementById('score-3')];

    $("#scoreBar").show();

    switch (highscoreArray.length) {
        case 0:

            break;
        case 1:
            $("#score-1").show();
            $("#score-2").hide();
            $("#remainingsquares").show();
            $("#filledsquares").show();

            break;
        case 2:
            $("#score-1").show();
            $("#score-2").show();
            $("#score-3").hide();
            $("#remainingsquares").show();
            $("#filledsquares").show();
            break;
        case 3:
            $("#score-1").show();
            $("#score-2").show();
            $("#score-3").show();
            $("#remainingsquares").show();
            $("#filledsquares").show();
            break;
    }

    var j = 1;
    for (i = 0; i < highscoreArray.length; i++) {
        place[i].style.color = highscoreArray[i].colour;
        place[i].innerHTML = ("#" + j + " score: " + highscoreArray[i].score);
        j++;

    }
}

function displayRemainingSqaure(value) {
    var remainingSqaure = document.getElementById('remainingsquares');
    remainingSqaure.innerHTML = ("Resterende blokjes: " + value)
}

function displayfilledSqaure(value) {
    var filledSqaure = document.getElementById('filledsquares');
    filledSqaure.innerHTML = ("Ingekleurde blokjes: " + value)
}

function displayRemainingSquares() {
    displayfilledSqaure((widthBlocks * heightBlocks) - squareCounter);
    displayRemainingSqaure(squareCounter);
}
