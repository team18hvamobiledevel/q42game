// Player object
var Player = function (x, y, direction, colour, colourLight) {
    this.x = x;
    this.y = y;
    this.direction = direction;
    this.colour = colour;
    this.colourLight = colourLight;
    this.ticks = 0;
    this.speed = 10;
    this.score = 0;

    this.flicker = false;
    this.flickerTicks = 0;
    this.flickerSpeed = 10;

    this.ticksLoop = 0;
    this.disconnected = false;
    this.ticks = 0;
    this.speed = 10;
    this.score = 0;

    //bigger upgrade
    this.isBigger = false;
    this.ticksLoop = 0;

    //bomb variables
    this.hasBomb = false;
    this.bombTicks = 0;
    this.bombTimer = 750;
    this.tap = false;
    this.bomFlickers = false;

    //shooting variables
    this.shootTimer = 60;
    this.shootTicks = 0;
    this.isShooting = false;

    this.shootingAreaRight = [
        [5,0], [5,1], [5,-1], [6,0], [6,1], [6,-1]
    ];

    this.shootingAreaLeft = [
        [-5,0], [-5,1], [-5,-1], [-6,0], [-6,1], [-6,-1]
    ];

    this.shootingAreaDown = [
        [1,5], [0,5], [-1,5], [1,6], [0,6], [-1,6]
    ];

    this.shootingAreaUp = [
        [1,-5], [0,-5], [-1,-5], [1,-6], [0,-6], [-1,-6]
    ];

    this.biggerShootingAreaRight = [
        [5,0], [5,1], [5,-1], [6,0], [6,1], [6,-1], [7,0], [7,1], [7,-1], [5,2], [6,2], [7,2]
        ];

    this.biggerShootingAreaLeft = [
        [-5,0], [-5,1], [-5,-1], [-6,0], [-6,1], [-6,-1], [-7,0], [-7,1], [-7,-1], [-5,2], [-6,2], [-7,2]
    ];

    this.biggerShootingAreaDown = [
        [1,5], [0,5], [-1,5], [1,6], [0,6], [-1,6],[1,7], [0,7], [-1,7], [2,5], [2,6], [2,7]
    ];

    this.biggerShootingAreaUp = [
        [1,-5], [0,-5], [-1,-5], [1,-6], [0,-6], [-1,-6],[1,-7], [0,-7], [-1,-7], [2,-5], [2,-6], [2,-7]
    ];


    this.showMap=false;
    this.mapCount=0;


    this.invincible = false;
    this.invincibleTicks = 0;
    this.invincibleSpeed = 7;
    this.disconnected = false;
    this.ticksInvincible = 0;
    this.isInvincible = false;

    this.hasFlicker = function () {
        this.flickerTicks++;
        if(this.flickerTicks == this.flickerSpeed){
            this.flicker = !this.flicker;
            this.flickerTicks = 0;
        }
        return this.flicker;
    };

    this.hasInvincible = function () {
        this.invincibleTicks++;
        if(this.invincibleTicks == this.invincibleSpeed){
            this.invincible = !this.invincible;
            this.invincibleTicks = 0;
        }
        return this.invincible;
    };

    this.move = function (player) {
        var x = 6;
        var areaUp = this.shootingAreaUp, areaRight = this.shootingAreaRight,
            areaDown = this.shootingAreaDown, areaLeft = this.shootingAreaLeft;

        //check what shooting area is needed
        if(player.isBigger){
            areaUp = this.biggerShootingAreaUp;
            areaRight = this.biggerShootingAreaRight;
            areaDown = this.biggerShootingAreaDown;
            areaLeft = this.biggerShootingAreaLeft;
            x = 12;
        } else {
            areaUp = this.shootingAreaUp;
            areaRight = this.shootingAreaRight;
            areaDown = this.shootingAreaDown;
            areaLeft = this.shootingAreaLeft;
            x = 6;
        }

        var rnd = Math.floor(Math.random()* x), areaX = 0, areaY = 0;
        var rnd2 = Math.floor(Math.random()* x), areaX2 = 0, areaY2 = 0;
        if (this.direction == 0) {
            areaX = areaUp[rnd][0];
            areaY = areaUp[rnd][1];
            areaX2 = areaUp[rnd2][0];
            areaY2 = areaUp[rnd2][1];
            this.moveUp();
        }

        if (this.direction == 1) {
            areaX = areaRight[rnd][0];
            areaY = areaRight[rnd][1];
            areaX2 = areaRight[rnd2][0];
            areaY2 = areaRight[rnd2][1];
            this.moveRight();
        }

        if (this.direction == 2) {
            areaX = areaDown[rnd][0];
            areaY = areaDown[rnd][1];
            areaX2 = areaDown[rnd2][0];
            areaY2 = areaDown[rnd2][1];
            this.moveDown();
        }

        if (this.direction == 3) {
            areaX = areaLeft[rnd][0];
            areaY = areaLeft[rnd][1];
            areaX2 = areaLeft[rnd2][0];
            areaY2 = areaLeft[rnd2][1];
            this.moveLeft();
        }

        if(this.isShooting) {
            if(this.shootTicks >= this.shootTimer) {
                this.isShooting = false;
                this.shootTicks = 0;
                POWERUPS_SPAWNED--;
            }
            if(player.isBigger){
                this.shooting(player, areaX2, areaY2);
            }
            this.shooting(player, areaX, areaY);
            this.shootTicks++;
        }

        this.ticks=0;
        this.ticksLoop += 1;
        this.ticksInvincible += 1;
    };

    this.moveUp = function () {
        this.y--;
        if(this.isBigger && this.y <= -1){
            this.y = Math.floor(screenHeight / squareSize) - 2;
        }
        if (this.y <= -1) {
            this.y = Math.floor(screenHeight / squareSize) - 1;
        }
    };
    this.moveRight = function () {
        this.x++;
        if (this.isBigger && this.x >= Math.floor(screenWidth / squareSize)-1){
            this.x = 0;
        }
        if (this.x >= Math.floor(screenWidth / squareSize)) {
            this.x = 0;
        }
    };
    this.moveDown = function () {
        this.y++;
        if (this.isBigger && this.y > Math.floor(screenHeight / squareSize) - 2) {
            this.y = 0;
        }
        if (this.y > Math.floor(screenHeight / squareSize) - 1) {
            this.y = 0;
        }
    };
    this.moveLeft = function () {
        this.x--;
        if (this.isBigger && this.x <= -1) {
            this.x = Math.floor(screenWidth / squareSize) - 2;
        }
        if (this.x <= -1) {
            this.x = Math.floor(screenWidth / squareSize) - 1;
        }
    };

    this.dropBomb = function (player) {
        //drop bomb
        //console.log(this.x + " " + this.y);
        var square = playField[this.x][this.y];
        //console.log(player.colourLight +" "+ square.player);
        if(this.bombTicks > this.bombTimer && this.hasBomb) {
            square.bombOwnedBy = player;
            square.isBomb = true;
            square.bombNotPlaced = false;
            square.bombTimer = 0;
            bombArray.push(square);

            this.bombTicks = 0;
            this.hasBomb = false;
        }
        if(this.tap && this.hasBomb) {
            square.bombOwnedBy = player;
            square.isBomb = true;
            square.bombNotPlaced = false;
            square.bombTimer = 250;
            square.flickerTimer = 10;
            drawPickUpBomb(square, this.colour);
            bombArray.push(square);
            this.hasBomb = false;
            this.bombTicks = 0;
            //???
        }
    };

    this.shooting =  function(player, areaX, areaY) {
        var cX = this.x + areaX,
            cY = this.y + areaY;

        //gunPop(1,squareSize,player,this.colour, scoreBarHeight);

        if (cX  > widthBlocks - 1) {
            cX = cX - widthBlocks;
        }
        else if (cX < 0) {
            cX = widthBlocks + cX;
        }
        if (cY  > heightBlocks - 1) {
            cY = cY - heightBlocks;
        }
        else if (cY  < 0) {
            cY = heightBlocks + cY;
        }

        var  sq = playField[cX][cY];
        var playerTwo = sq.player;

        if(!sq.player){
          squareCounter --;
        }
        //check if bullet hits an enemy
        if(sq.player && sq.standingOn && !playerTwo.isInvincible) {
            if(playerTwo.hasBomb){
              playerTwo.bombTimer = 0;
            }
            respawnPlayer(playerTwo);
        }



        checkTile(cX, cY, player);
        sq.player = player;

        //check if bullet hits a bomb and own it
        if (sq.isBomb && !sq.canPickUp && sq.bombNotPlaced) {
            sq.bombOwnedBy = player;
        }

        if(!sq.bombNotPlaced){
           sq.bombTimer = 0;
       }


        //check if it hits a gun powerup and redraws it
        if(sq.isShootingPowerUp) {
            console.log("hit gun");
            this.drawSquare(sq, this.colourLight);
            this.drawGun(sq, "#000");
        }
        //check if it hits a pickupbomb and redraws it
        if(sq.canPickUp) {
            console.log("hit bomb");
            this.drawPickUpBomb(sq, "#000");
        }

        var fromX = (this.x*squareSize),
            fromY = (this.y*squareSize) + scoreBarHeight,
            toX = (cX*squareSize),
            toY = (cY*squareSize)+scoreBarHeight;

        gunPop(player.colour, fromX, fromY, toX, toY, player.direction);

        drawSquare(sq, this.colourLight);
    };
    this.bomFlicker = function () {
        this.flickerTicks++;
        if(this.flickerTicks == this.flickerSpeed){
            this.bomFlickers = !this.bomFlickers;
            this.flickerTicks = 0;
        }

        return this.bomFlickers;
    };
};
