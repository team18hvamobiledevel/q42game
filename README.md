# Q42 MMOPG

De Q42 MMOPG was made by students of the HvA, [Team-18](http://www.team18.nl/), during their thematic semester Mobile Development.
### Version 1.0.0
### Tech

Q42 MMOPGuses a number of open source projects to work properly:

* [node.js] - evented I/O for the backend
* [Socket.io]- for the server and client communication with the display
* [jQuery] - duh
* [Hammer.io] - for touch input


### Installation

Q42 MMOPG requires [Node.js](https://nodejs.org/) v4+ to run.

```sh
$ git clone https://bitbucket.org/team18hvamobiledevel/q42game.git q42mmopg
$ cd q42mmopg
$ npm install
$ npm start
```
### Development

Want to contribute? Great!

Open your favorite Terminal and run these commands.

```sh
$ git clone https://bitbucket.org/team18hvamobiledevel/q42game.git q42mmopg
$ cd q42mmopg
$ npm install
$ npm start
```

### Problems and Solutions

#TODO ADD SOME STUFFS HERE


License
----

DONT STEAL OUR SHIT K ITS OURS!

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [node.js]: <http://nodejs.org>
   [jQuery]: <http://jquery.com>
   [Socket.io]: <http://socket.io/>
   [Hammer.io]: <http://hammerjs.github.io/>
